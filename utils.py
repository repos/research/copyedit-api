import requests
import re
import json
# import nltk
from bs4 import BeautifulSoup # necessary for parsing HTML
import language_tool_python
from collections import defaultdict

def get_article_html(page_title, lang):
    """
    Get the HTML-version of an article of a Wikipedia.
    This uses the REST API https://en.wikipedia.org/api/rest_v1/#/Page%20content/get_page_html__title_
    """
    headers = {"User-Agent": "MGerlach_(WMF): research-copyediting"}
    api_url = "https://%s.wikipedia.org/api/rest_v1/page/html/%s"%(lang,page_title)
    params = {
        "format": "json",
    }
    article_html = ""
    try:
        response = requests.get(api_url, headers=headers, params=params)
        article_html = response.content.decode("utf-8")
    except:
        pass
    return article_html

def html2sentences(article_html):
    '''
    Convert html to a list of sentences.
    '''
    soup = BeautifulSoup(article_html, 'html.parser')
    list_p = soup.find_all('p')
    list_sentences = []
    for p in list_p:
        # get the id of the paragraph from the attributes (in case it exists)
        try:
            p_id = p.attrs.get("id")
        except AttributeError:
            p_id = None

        # remove if the paragraph doesnt have an id (example: https://en.wikipedia.org/wiki/Worship_Him)
        if p_id==None:
            continue
        
        ## if the piece has an additional attribute it is likely not from the body of the text but translcuded.
        p_keep = True
        for a in p.attrs:
            if a!="id":
                p_keep=False
        if p_keep == False:
            continue

        ## remove if text is from blockquote
        p_parent = p.parent
        if p.parent.name != "section":
            continue
        
        
        p_text = ""

        list_pc = []
        i1,i2=-1,0 # index markers for the text elements
        ## iterating through the individual elements of a piece
        for pc in p:
            i1=i2
            ## if it is pure text, the children does not contain the attrbute attrs
            pc_text = pc.text.replace("\n"," ")#.replace("  "," ")
            # replace multiple whitespaces by single to avoid whitespace error
            _RE_COMBINE_WHITESPACE = re.compile(r"\s+")
            pc_text = _RE_COMBINE_WHITESPACE.sub(" ", pc_text)

            if pc_text=="":
                continue

            # get the attributes of the text piece (if attributes exist)
            pc_id = None
            pc_attr = None
            pc_name = None
            try:
                pc_id = pc.attrs.get("id")
                pc_attr = pc.attrs
                pc_name = pc.name
                if "id" in pc.attrs:
                    # remove the id-attribute for later filtering
                    pc.attrs.pop("id")
            except AttributeError:
                pass

            # ignore references (and some templates)
            if pc_name == "sup":
                continue
            # style annotations
            if pc_name == "style":
                continue
#                 if "reference" in pc_attr.get("class",[]):
            i2 = i1+len(pc_text)
            p_text += pc_text
            dict_pc = {"text":pc_text, "attr":pc_attr, "name":pc_name, "i_start":i1, "i_end":i2, "pc_id":pc_id}
            list_pc += [dict_pc]

        if len(list_pc)>0 and len(p_text)>100:
            list_sentences+=[{"p_id": p_id, "text": p_text, "text_pc": list_pc}]
    return list_sentences

def tokenize_sentence(text):
    '''
    splits sentences (or paragraphs)
    '''
    # full stop symbol in bengali
    text = text.replace("।", ".\n")
    for line in text.split("\n"):
        yield line
        # for sent in nltk.sent_tokenize(line):
        #     ## if sentences are very short, they are often just artifacts.
        #     if len(sent)>=10:
        #         yield sent

# run LanguageTool
def map_lang_lt(lang):
    '''
    Map wikipedia-project's language to the language used by LanguageTool.
    For example, simple (from simplewiki) gets mapped to en.
    '''
    dict_map_lang = {"simple":"en"}
    # if there is no entry in the dictionary, we return the input lang
    lang_lt = dict_map_lang.get(lang,lang)
    return lang_lt

def get_tool_lt(lang, remote_server = None):
    '''
    Start LanguageTool instance with lang.
    Default is local server.
    Otherwise, specify remote_server.
    '''
    lang_lt = map_lang_lt(lang) # map the wiki-language to the language-code in LanguageTool

    # if remote server not specified, use local server
    if remote_server == None:
        tool = language_tool_python.LanguageTool(lang_lt)
    else:
        # try the remote server otherwise fall back on local
        try:
            tool = language_tool_python.LanguageTool(lang_lt, remote_server=remote_server)
        except:
            # in case of
            tool = language_tool_python.LanguageTool(lang_lt)
    return tool

def map_match2dict(match):
    dict_match = {
        "ruleId":match.ruleId,
        "message":match.message,
        "context":match.context,
        "matchedText":match.matchedText,
        "replacements":match.replacements,
        "offsetInContext":match.offsetInContext,
        "offset":match.offset,
        "errorLength":match.errorLength,
        "category":match.category,
        "ruleIssueType":match.ruleIssueType,
        "sentence":match.sentence,
        "err_i1":match.offset,
        "err_i2":match.offset+match.errorLength,
        "err_text":match.matchedText

    }
    return dict_match

def get_errors_lt_text(text, tool):
    try:
        matches = tool.check(text)
    except:
        # in case the call throws an exception we return an empty list
        matches = []
    # format errors into a list of dicts
    matches_formatted = [map_match2dict(match) for match in matches]
    return matches_formatted

def get_errors_lt(paragraphs, tool):
    dict_par_errors = {}
    for par in paragraphs:
        text = par["text"]
        p_id = par["p_id"]
        matches = get_errors_lt_text(text, tool)
        if len(matches)>0:
            dict_par_errors[p_id] = matches
    return dict_par_errors

def get_errors_cm(paragraphs, dict_cm):
    """
    Get errors from dictionary of common misspellings {"misspellded word":"correct word"}
    """
    list_s = list(dict_cm.keys())
    str_match = "|".join([re.escape(h) for h in list_s])
#     pattern = re.compile(r"\b({0})\b".format(str_match))
    # \u0980-\u09FF is bengali 
    pattern = re.compile(r"(?<![\w\u0980-\u09FF])({0})(?![\w\u0980-\u09FF])".format(str_match))

    dict_par_errors = {}
    for par in paragraphs:
        text = par["text"]
        p_id = par["p_id"]
        matches = []
        for m in re.finditer(pattern,text):
            word = m.group(0)
            wordpos = m.start()
            suggestion = dict_cm.get(word,"")
            err = {
                "replacements": suggestion,
                "err_i1":wordpos,
                "err_i2":wordpos+len(word),
                "err_text":word
            }
            matches += [err]
        if len(matches)>0:
            dict_par_errors[p_id] = matches
    return dict_par_errors

# filters of errors
def check_err_whitespace(err):
    """
    will mark errors as False if:
    - err_text only consists of whitespaces
    """
    is_err = True
    err_text = err["err_text"]
    if err_text.strip()=="":
        is_err = False
    return is_err

def check_err_upper(err):
    """
    will mark error as False if:
    - err_text starts with capital letter
    """
    is_err = True
    err_text = err["err_text"]
    if err_text[0].isupper()==True:
        is_err = False
    return is_err

def check_err_len(err, len_min = 0):
    """
    will mark error as False if:
    - err_text exceeds a given length (default: 0)
    """
    is_err = True
    err_text = err["err_text"]
    if len(err_text)<len_min:
        is_err = False
    return is_err

def check_err_span(err_i1,err_i2,i1,i2):
    """
    check for overlap of error-span (err_i1,err_i2) with span in the text
    """
    is_err = True
    if (err_i1 >= i1 and err_i1<i2) or (err_i2 >= i1 and err_i2<i2) or (err_i1 <= i1 and err_i2>=i2):
        is_err = False
    return is_err

def check_err_brackets(err,text):
    """
    will mark error as False if:
    - err_span overlaps with any content in brackets
    """
    pattern = r'\(.*?\)'
    is_err = True
    err_i1 = err["err_i1"]
    err_i2 = err["err_i2"]
    for m in re.finditer(pattern,text):
        i1 = m.start()
        i2 = m.end()
        if check_err_span(err_i1,err_i2,i1,i2) == False:
            is_err=False
            break
    return is_err

def check_err_quotes_double(err,text):
    """
    will mark error as False if:
    - err_span overlaps with any content in double quotes
    """
    pattern = r'(^|\W)("|„|“)(.+?)(“|"|”)(\W|$)'
    # ("|„|“)(.+?)(“|"|”) anything in quites
    # (^|\W) before the start of the quote there is a non-word character (\W) or its the beginning of the string (^)
    # (\W|$) after the end of the quote there is a non-word character (\W) or its the end of the string (&)
    
    is_err = True
    err_i1 = err["err_i1"]
    err_i2 = err["err_i2"]
    for m in re.finditer(pattern,text):
        i1 = m.start()
        i2 = m.end()
        if check_err_span(err_i1,err_i2,i1,i2) == False:
            is_err=False
            break
    return is_err

def check_err_quotes_single(err,text):
    """
    will mark error as False if:
    - err_span overlaps with any content in single quotes
    - note that this avoids text that appears in between two apostrophes
    """
    pattern = r"(^|\W)('|’|‘)(.+?)('|’|‘)(\W|$)"
    # ('|’|‘)(.+?)('|’|‘) anything in quotes
    # (^|\W) before the start of the quote there is a non-word character (\W) or its the beginning of the string (^)
    # (\W|$) after the end of the quote there is a non-word character (\W) or its the end of the string (&)
    
    is_err = True
    err_i1 = err["err_i1"]
    err_i2 = err["err_i2"]
    for m in re.finditer(pattern,text):
        i1 = m.start()
        i2 = m.end()
        if check_err_span(err_i1,err_i2,i1,i2) == False:
            is_err=False
            break
    return is_err

def check_err_attr(err,text_attr):
    """
    will mark error as False if:
    - err_span overlaps with any text containing an attribute
    """
    is_err = True
    err_i1 = err["err_i1"]
    err_i2 = err["err_i2"]
    for pc in text_attr:
        if pc["attr"] != None:
            i1 = pc["i_start"]
            i2 = pc["i_end"]
            if check_err_span(err_i1,err_i2,i1,i2) == False:
                is_err = False
                break
    return is_err

def check_err_substring(err,text_substring):
    """
    will mark error as False if:
    - err_text matches any substring of text_substring
    """
    is_err = True
    err_text = err["err_text"]
    if err_text in text_substring:
        is_err = False
    return is_err

def get_text_substring(paragraphs):
    list_ignore = []
    for par in paragraphs:
        text = par["text"]
        text_pc = par["text_pc"]
        for pc in text_pc:
            if pc["attr"] != None:
                list_ignore+=[pc["text"].lower()]
    str_ignore = '|'.join(list_ignore) ## single string with all terms from the ignore list
    return str_ignore

def check_err_hyphen(err,text):
    is_err = True
    err_i1 = err["err_i1"]
    err_i2 = err["err_i2"]
    # hypen in the error-text
    if "-" in text[err_i1:err_i2]:
        is_err = False
    # hyphen just before the error-text
    if err_i1>0 :
        if text[err_i1-1] == "-":
            is_err = False
    # hyphen just after the error-text
    if err_i2<len(text):
        if text[err_i2] == "-":
            is_err = False
    # if hyphen appears in the corrections
    suggestions = err.get("replacements",[])
    if isinstance(suggestions,str):
        suggestions = [suggestions]
    for str_suggestion in suggestions:
        if "-" in str_suggestion:
            is_err=False
    return is_err

def check_err_singlequote_lead_trail(err,text):
    """
    will mark error as False if:
    - err_text preceeded and followed by single quote 
    note: single quotes can appear in isolation so not possible via regex
    """
    is_err = True
    err_i1 = err["err_i1"]
    err_i2 = err["err_i2"]
    list_single_quotes = ["'","’"]
    if err_i1>0 and err_i2<len(text):
        if text[err_i1-1] in list_single_quotes and text[err_i2] in list_single_quotes:
            is_err = False
    return is_err

def check_err_sic(err,text):
    """
    will mark error as False if:
    - the text contains "sic" anywhere https://en.wikipedia.org/wiki/Sic
    """
    is_err=True
    pattern = r"(?<![\w\u0980-\u09FF])(sic)(?![\w\u0980-\u09FF])"
    if re.search(pattern,text,re.IGNORECASE)!=None:
        is_err = False
    return is_err

def check_err_lt_cat(err, list_categories = []):
    """
    will mark error as False if:
    - err_category is included in the list of categories (default: empty list)
    - example: "PUNCTUATION"
    """
    is_err=True
    err_cat = err.get("category")
    if err_cat in list_categories:
        is_err = False
    return is_err

def check_err_lt_type(err, list_types = []):
    """
    will mark error as False if:
    - err_type is included in the list of rules (default: empty list)
    - example: "typographical"
    """
    is_err=True
    err_type = err.get("ruleIssueType")
    if err_type in list_types:
        is_err = False
    return is_err

def check_err_lt_rule(err, list_rules = []):
    """
    will mark error as False if:
    - err_rule is included in the list of rules (default: empty list)
    - example: "MISSING_COMMA_AFTER_INTRODUCTORY_PHRASE"
    """
    is_err=True
    err_rule = err.get("ruleId")
    if err_rule in list_rules:
        is_err = False
    return is_err

def filter_errors(dict_par_errors, paragraphs, len_min=0, list_categories_filter=[], list_types_filter=[], list_rules_filter=[]):
    """
    Filter the dictionary of errors of the form {par_id: [error]}
    Optional arguments:
    - len_min: minimum length of the error-text
    - list_categories_filter: error-categories from LanguageTool to filter (e.g. ["PUNCTUATION"])
    - list_types_filter: error-types from LanguageTool to filter (e.g. ["typographical"])
    - list_rules_filter: error-rules from LanguageTool to filter (e.g. ["MISSING_COMMA_AFTER_INTRODUCTORY_PHRASE"])
    """
    dict_par_errors_filtered = defaultdict(list)
    text_substring = get_text_substring(paragraphs)
    for par in paragraphs:
        text = par["text"]
        text_attr = par["text_pc"]
        p_id = par["p_id"]
        errors = dict_par_errors.get(p_id,[])
        for err in errors:
            if check_err_whitespace(err)==False:
                continue
            if check_err_upper(err)==False:
                continue
            if check_err_len(err, len_min=len_min)==False:
                continue
            if check_err_brackets(err,text)==False:
                continue
            if check_err_quotes_double(err,text)==False:
                continue
            if check_err_quotes_single(err,text)==False:
                continue
            if check_err_singlequote_lead_trail(err,text)==False:
                continue
            if check_err_attr(err,text_attr)==False:
                continue
            if check_err_substring(err,text_substring)==False:
                continue
            if check_err_hyphen(err,text)==False:
                continue
            if check_err_sic(err,text)==False:
                continue
            if check_err_lt_cat(err,list_categories=list_categories_filter)==False:
                continue
            if check_err_lt_type(err,list_types=list_types_filter)==False:
                continue
            if check_err_lt_rule(err,list_rules=list_rules_filter)==False:
                continue
            dict_par_errors_filtered[p_id] += [err]
    return dict_par_errors_filtered

def format_errors(dict_par_errors, paragraphs):
    list_dict_out = []
    dict_out = {}
    for par in paragraphs:
        text = par["text"]
        p_id = par["p_id"]
        errors = dict_par_errors.get(p_id,[])
        for err in errors:
            dict_out = {}
            for k,v in err.items():
                dict_out[k] = v
            dict_out["p_id"] = p_id
            dict_out["p_text"] = text
            list_dict_out+=[dict_out]
    return list_dict_out