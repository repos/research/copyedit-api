# copyedit-api

An exploratory API to surface copyedits (spelling, grammar, etc mistakes) in Wikipedia articles.

## Usage

The API takes two required arguments:
- "lang": Wikipedia language version, e.g. "en" or "simple"
- "title": Title of the page

For example, for the article of [Chiara Cordelli in English Wikipedia](https://en.wikipedia.org/wiki/Chiara_Cordelli) we would do the following call:
- https://copyedit.toolforge.org/api/v1/lt?lang=en&title=Chiara_Cordelli

Currently, this works for the following languge version of Wikipedia (based on the languages supported by [LanguageTool](https://languagetool.org/)):
`ar, ast, be, br, ca, da, de, el, en (and simple), eo, es, fa, fr, ga, gl, it, ja, km, nl, pl, pt, ro, ru, sk, sl, sv, ta, tl, uk, zh`


The API takes several optional arguments related to the filtering of the errors which you can pass:
- len_min: minimum length of the error-text (default:0)
- filter_category: error-categories from LanguageTool to filter (e.g. "PUNCTUATION"); separate with "|"; default: None
- filter_type: error-types from LanguageTool to filter (e.g. "typographical"); separate with "|"; default: None
- filter_rule: error-rules from LanguageTool to filter (e.g. "MISSING_COMMA_AFTER_INTRODUCTORY_PHRASE"); separate with "|"; default: None

## Details

### LanguageTool
We use LanguageTool to detect spelling and error mistakes.
We set up [remote server](https://dev.languagetool.org/http-server) running our own instance of LanguageTool on cloud-vps ([code for setup](https://github.com/wikimedia/research-api-endpoint-template/tree/language-tool)).
We can interact with this endpoint via:
- https://copyedits.wmcloud.org/v2/check?language=en&text=my+text


### Filtering of errors
We run LanguageTool only on the text of an article:
- we first get its HTML-version from querying the [REST-API](https://en.wikipedia.org/api/rest_v1/#/Page%20content/get_page_html__title_)
- we only keep text that appears as part of a paragraph \<p>...\</p>
- we discard paragraphs with additional attributes as these typically do not come from the body of the article but are transcluded from, e.g., templates
- we discard paragraphs that are not a child of an element with name "section". this filters text that is, e.g., a blockquote (text with indentation) or appears in multi-column tables (often used for text in different languages from lyrics or poems)
- we parse through all elements of a paragraph and extract the displayed text as well as additional attributes such as whether it is a link or bold or italic (we discard elements with the \<sup> attribute as this often contains links to references such as [11])
- we then pass the filtered text of each paragraph to LanguageTool to find errors

We then have to filter the errors as LanguageTool typically is very sensitive and yields many false positives. For example, it often highlights names of entities that are linked (such as names of persons) as spelling errors ([example](https://copyedits.wmcloud.org/v2/check?language=en-US&text=Chiara%20Cordelli%20is%20an%20associate%20professor%20in%20the%20Department%20of%20Political%20Science%20at%20the%20University%20of%20Chicago.) using the first sentence of the article above).
We filter errors where:
- brackets: error-text appears in brackets
- quotes: error-text appears in quotes (single and double), error-text has a leading or trailing single quote
- hyphen: error-text contains a hyphen or has leading or trailing hyphen or the replacement-text contains a hyphen
- capital first letter: error-text' first letter is uppercase
- sic: the text in which the error appears contains the word "sic" (see: https://en.wikipedia.org/wiki/Sic)
- error-text does not exceed a minimum length (can be specified; default: 0)
- the error-text matches any of the substrings which have additional attributes (links, bold, italics, etc)
- the position of the err-text of the errors overlaps with the position of substrings which have additional attributes (links, bold, italics, etc)
- error from LanguageTool matches a specific category/type/rule (can be specified; default: None)


These filters reduce the number of false positives substantially. As we investigate the copyedit-tool in the context of Wikipedia-articles more thoroughly, we will probably adapt these filtering heuristics.
