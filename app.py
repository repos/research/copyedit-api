import re
import requests
from flask import Flask, request, jsonify, render_template
import utils
import language_tool_python

app = Flask(__name__)
app.config["DEBUG"] = True
app.config['JSON_SORT_KEYS'] = False
CUSTOM_UA = 'copyedits api -- mgerlach@wikimedia.org'

## suported languages for languagetool
list_lang_lt = ["ar", "ast", "be", "br", "ca", "da", "de", "el", "en", "eo", "es", "fa", "fr", "ga", "gl", "it", "ja", "km", "nl", "pl", "pt", "ro", "ru", "sk", "sl", "sv", "ta", "tl", "uk", "zh"]
## remote server
remote_server = "https://copyedits.wmcloud.org"

print("Try: http://127.0.0.1:5000/api/v1/lt?lang=en&title=Chiara_Cordelli")

## locally: flask run

@app.route('/')
def index():
    return 'Server Works!'

@app.route('/api/v1/lt', methods=['GET'])
def get_copyedits_lt():
    # args
    lang = request.args.get('lang')
    page_title = request.args.get('title')
    filter_errors = request.args.get('filter',True)
    len_min = int(request.args.get('len_min',0))
    list_categories_filter = [w for w in request.args.get('filter_category',"").split("|") if w!=""]
    list_types_filter = [w for w in request.args.get('filter_type',"").split("|") if w!=""]
    list_rules_filter = [w for w in request.args.get('filter_rule',"").split("|") if w!=""]

    # map wiki-language to lt-language
    lang_lt = utils.map_lang_lt(lang)

    # get HTML
    article_html = utils.get_article_html(page_title, lang)
    # article_html is empty string if something went wrong
    if article_html == "":
        return jsonify({"Error":"Couldnt get the article you requested. Check the title"})
    elif lang_lt not in list_lang_lt:
        return jsonify({"Error":"Language currently not supported. Try another language." })
    else:
        # pre-process HTML to get text
        paragraphs = utils.html2sentences(article_html)
        # run LanguageTool to get errors
        tool = utils.get_tool_lt(lang, remote_server=remote_server) # get languagetool instance
        dict_par_errors = utils.get_errors_lt(paragraphs,tool)
        # filter errors
        if filter_errors==True:
            dict_par_errors_filtered = utils.filter_errors(dict_par_errors, paragraphs,len_min=len_min, list_categories_filter=list_categories_filter, list_types_filter=list_types_filter, list_rules_filter=list_rules_filter)
        else:
            dict_par_errors_filtered = dict_par_errors
        # formatting
        errors_return = utils.format_errors(dict_par_errors_filtered, paragraphs)

        if len(errors_return)>0:
            return jsonify(errors_return)
        else:
            return jsonify({"Warning":"No copyedits found. Try another article."})

if __name__ == '__main__':
    '''
    '''
    app.run(host='0.0.0.0')
